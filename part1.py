oleg_salary = [600, 1000, 1200, 950, 700]
olga_salary = [1500, 300, 400, 0, 500]
 
print('Олег, бюджет: ', sum(oleg_salary))
print('Ольга, бюджет: ', sum(olga_salary))
 
family_budget = sum(oleg_salary) + sum(olga_salary)
print('Общий бюджет: ', family_budget) 
 
flight_costs = 50
 
trips_count = 3
 
trip_length_1 = 10
trip_length_2 = 20
trip_length_3 = 10
 
day_cost = 20
flights_per_trip = 2
 
trip_cost = (trip_length_1 + trip_length_2 + trip_length_1) * day_cost
trip_cost += flight_costs * trips_count * flights_per_trip
 
print ('Стоимость поездки: ', trip_cost)
 
month_before_trip = 3
save_per_month = trip_cost / month_before_trip

print('Нужно откладывать каждый месяц', save_per_month)

if family_budget < trip_cost:
  deficit = trip_cost - family_budget
  print('Дефицит бюджета: ', deficit, 'евро')
else:
  proficit = family_budget - trip_cost
  print('У нас остается: ', proficit, 'евро')
  
# Выводить текущий курс евро к рублю (считаем, что он не меняется).
eur_rub_ratio = 59.19
print ('Курс евро к рублю: ', eur_rub_ratio)

#Вывести стоимость проживания не только в евро, но и в рублях.
print ('Стоимость проживания в рублях: ', (trip_length_1 + trip_length_2 + trip_length_1) * day_cost * trips_count * eur_rub_ratio, 'руб.')

# Допустим у нас есть бюджет на поездку, выяснить, не выходим ли мы за него, если выходим, то вывести предупреждение.
budget_for_travel = family_budget * 0.20

if trip_cost > budget_for_travel:
  print ('В семейном бюджете недостаточно средств для поездки, недостающая сумма: : ', trip_cost - budget_for_travel, 'евро')
else:
  print ('В семейном бюджете для поездки средств достаточно, излишек: ', budget_for_travel - trip_cost, 'евро')
 
# Предположим, что каждый из нас начал вести список расходов. Вывести сумму, которую потратил каждый и сколько осталось в общем бюджете. 

oleg_expenses = 0
olga_expenses = 0

days_spent = 3;
cnt = 0

while cnt < days_spent:
  oleg_expenses += 2
  olga_expenses += 4
  cnt += 1

print ('Олег потратил: ', oleg_expenses, 'евро')
print ('Ольга потратила: ', olga_expenses, 'евро')
print ('В общем семейном бюджете осталось: ', family_budget - oleg_expenses - olga_expenses, 'евро')
print ('В бюджете на поездку осталось: ', budget_for_travel - oleg_expenses - olga_expenses, 'евро')

