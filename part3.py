# Given an unsorted list of points on Cartisian plane, find 2 points that can from a rectangle with the largest area
import random
 
class Point: # what does "too few public methods mean?"
  def __init__(self, x, y):
    self._x = x
    self._y = y
  def show(self):
    print("x = ", self._x, " y = ", self._y)
 
def rect_brute_force(lst): # about O(n^2)
  s = 0
  sz = len(lst)
  for i in range (0, sz): # why does it say redifining i from outer scope?
    for j in range (i + 1, sz):
      a = abs(lst[i]._x - lst[j]._x)
      b = abs(lst[i]._y - lst[j]._y)
      if(s < a*b):
        s = a*b
        p1 = lst[i]
        p2 = lst[j]
  return [p1, p2]   
 
def rect_efficient(lst): # about O(2n) but no always works correctlt, wanna something like that fast
  p1 = lst[0]
  sz = len(lst)
  s = 0
  for i in range (1, sz): # find the most distant point, yet the most distant point does not always gives the largest rectangle 
    if(abs(lst[i]._x * lst[i]._y) > abs(p1._x * p1._y)):
      p1 = lst[i]
  for i in range (0, sz): # find the one that with most distant form the largest rectangle 
    a = abs(lst[i]._x - p1._x)
    b = abs(lst[i]._y - p1._y)
    if(s < a*b):
      s = a*b
      p2 = lst[i]
  return [p1, p2]

# get random points
points = []
for i in range (0, 10):
  points.append(Point(random.randrange(-20, 20, 2), random.randrange(-20, 20, 2)))
  
p = rect_brute_force(points)
p[0].show()
p[1].show()
 
print('')
 
p = rect_efficient(points) 
p[0].show()
p[1].show()